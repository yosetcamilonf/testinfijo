/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;
import Util.seed.Cola;
import Util.seed.Pila;

/**
 * Programa imprime si una cadena es el inverso de su subcadena
 * cuando son separadas por &
 * @author DOCENTE
 */
public class TestInfijo {
    
    public static void main(String[] args) {
        String cadena="UFPS&SPFU"; //--> true
        /**
         * LO QUE ESTÁ ANTES DEL & SE COLOCA EN UNA COLA
         * LO QUE ESTÁ DESPUÉS EN UNA PILA
         * Y SE COMPARA LA PILA Y LA COLA
         */
        String[] datos = cadena.split("&");
        Pila<Character> p = new Pila();
        Cola<Character> c = new Cola();
        
        for(char ch : datos[0].toCharArray()){
            p.push(ch);
        }
        
        for(char ch : datos[1].toCharArray()){
            c.enColar(ch);
        }
        
        boolean isInverted = false;
        
        while(!p.isEmpty()){
            if(p.pop()==(c.deColar())){
                isInverted = true;
                break;
            }
        }
        
        if(isInverted){
            System.out.println("es el inverso");
        }else{
            System.out.println("no es el inverso");
        }
        
        
    }
    
}
